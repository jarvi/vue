import Vue from "vue"
import App from "./App.vue"
import VueRouter from "vue-router"
import VueMoment from "vue-moment"
import moment from "moment"
import "moment/locale/es"

import HelloWorld from "../src/components/HelloWorld.vue"
import lastArticles from "../src/components/lastArticle.vue"
import pagina from "../src/components/pagina.vue"
import blog from "../src/components/blog.vue"
import formulario from "../src/components/formulario.vue"
import ErrorComponent from "../src/components/errorComp.vue"
import MiComponente from "../src/components/miComponent.vue"
import peliculas from "../src/components/peliculas.vue"
import search from "../src/components/search.vue"
import redirect from "../src/components/redirect.vue"
import articleDetail from "../src/components/article.vue"
import createArticle from "../src/components/createArticle/createArticle.vue"
import editArticle from "../src/components/editArticle.vue"


Vue.config.productionTip = false
Vue.use(VueRouter)
Vue.use(VueMoment, {moment})

const routes = [
  {path: "/home", component: lastArticles},
  {path: "/ultimos-articulos", component: lastArticles},
  {path: "/pagina/:id?", name: "pagina", component: pagina},
  {path: "/blog/:arg?", name: "blog", component: blog},
  {path: "/formulario", component: formulario},
  {path: "/buscador/:searchString", component: search},
  {path: "/crear-articulo", name:'create', component: createArticle},
  {path: "/editar/:id", name:'edit', component: editArticle},
  {path: "/redirect/:searchString", component: redirect},
  {path: "/articulo/:id", name: "article", component: articleDetail},
  {path: "/hello", component: HelloWorld},
  {path: "/componente", component: MiComponente},
  {path: "/movies", name: "m", component: peliculas},
  {path: "/", component: lastArticles},
  {path: "*", component: ErrorComponent}
]

const router = new VueRouter({
  routes,
  mode: "history"
})
new Vue({
  router,
  render: h => h(App)
}).$mount("#app")
